// Modify this file to change what commands output to your statusbar, and recompile using the make command.

static const Block blocks[] = {
  /* Icon */ /*Command*/					  /*Update Interval*/	/*Update Signal*/
  {"📁",  "du / -h | tail -n 1 | sed -e 's/\t.*//'",			120,		 0},
  {"⬆️ ",	 "/home/pug/.local/bin/upt",					   60,		   0},
  {"⏰", "date +'%a, %B %d %-l:%M:%S %p'",				  1,		 0},
};

// sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
